sgn :: Int -> Int
sgn n = if n < 0
        then -1
        else if n==0
             then 0
             else 1

absInt :: Int -> Int
absInt n = if(n < 0) then (-n)
           else n

min2Int :: (Int, Int) -> Int
min2Int (x, y) = if(x > y) then y
                 else x

min3Int :: (Int, Int, Int) -> Int
min3Int (x, y, z) = if(x > y) then if (y > z) then z
                                   else y
                    else if(x > z) then z
                         else x
min33Int :: (Int, Int, Int) -> Int
min33Int (x, y, z)= if(min2Int (x, y) > min2Int (y, z)) then min2Int (y, z)
                    else min2Int (x, y)

toUpper :: Char -> Char
toUpper x = toEnum ((fromEnum x) - 32)

toLower :: Char -> Char
toLower x = toEnum ((fromEnum x) + 32)
