roots :: (Double, Double, Double) -> (Double, Double)
roots (a, b, c) = ( (-b -d) / e, (-b + d) / e )
    where d = sqrt ( b * b - 4 * a * c)
          e = 2 * a

unitVec2D :: (Double, Double) -> (Double, Double)
unitVec2D (a, b) = (a / l, b / l)
    where l = sqrt( a ^ 2 + b ^ 2)

triangleHeron :: (Double, Double, Double) -> Double
triangleHeron (a, b, c) = sqrt( p * (p - a) * (p - b) * (p - c) )
    where p = 1 / 2 * (a + b + c)
