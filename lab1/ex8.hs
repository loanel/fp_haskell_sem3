not' :: Bool -> Bool
not' b = case b of
          True  -> False
          False -> True

absInt :: Int -> Int
absInt n =
    case (n >= 0) of
        True -> n
        _    -> -n

isItTheAnswer :: String -> Bool
isItTheAnswer s =
    case s of
      "Love" -> True
      _      -> False

or' :: (Bool, Bool) -> Bool
or' (x, y) =
    case (x, y) of
      (False, False) -> False
      (_,_)          -> True

and' :: (Bool, Bool) -> Bool
and' (x, y) =
    case (x, y) of
      (True, True) -> True
      (_,_)        -> False

nand' :: (Bool, Bool) -> Bool
nand' (x, y) = not' (and' (x, y))

xor' :: (Bool, Bool) -> Bool
xor' (x, y) =
    case (x, y) of
      (True, False) -> True
      (False, True) -> True
      (_, _)        -> False
