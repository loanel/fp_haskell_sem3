sum'2 :: Num a => [a] -> a
sum'2 xs = loop 0 xs
 where loop acc []     = acc
       loop acc (x:xs) = loop (x + acc) xs


sum'3 :: Num a => [a] -> a
sum'3 = loop 0
 where loop acc []     = acc
       loop acc (x:xs) = loop (x + acc) xs

prod'2 :: Num a => [a] -> a
prod'2 = loop 1
 where loop acc [] = acc
       loop acc (x:xs) = loop (x*acc) xs

selectEven :: Integer a => [a] -> [a]
selectEven = loop []
  where loop acc [] = acc
        loop acc (x:xs) = if (x `mod` 2 == 0) then loop (acc : x) xs
                          else loop acc xs
zipWith :: (a -> b -> c) -> [a] -> [b] -> [c]
