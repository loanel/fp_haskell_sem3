isPalindrome :: [Char] -> Bool
isPalindrome s = if (s == reverse s)  then True
                                      else False

getElemAtIdx :: Int -> [Char] -> Char
getElemAtIdx x xs = head (drop (x-1) xs)


capitalize :: [Char] -> [Char]
capitalize w = toUpper (head w) : tail w
